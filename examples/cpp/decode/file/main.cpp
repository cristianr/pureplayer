/*
 * flacplay : A simple memory-based FLAC stream decoder and player
 * 
 * Coded by Kostas Nakos (knakos .at. gmail.com), Feb 2011
 *
 */

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <MMSystem.h>
#include <Mmreg.h>
#include <mmdeviceapi.h>
#include <Audioclient.h>
#include <avrt.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "FLAC/stream_decoder.h"

static int dummycheck = 0;
static int metaerror = 0;

static FLAC__uint64 total_samples = 0;
static unsigned sample_rate = 0;
static unsigned channels = 0;
static unsigned bps = 0;

static FLAC__uint8 *buffer = NULL;
static FLAC__uint64 buffer_size = 0;
static HWAVEOUT wout;
static int woutBuffnum;
static WAVEHDR *woutBuffers;
static WAVEFORMATEXTENSIBLE wfe;
static WAVEFORMATEX *wf;

static char * dot;
static int pos;
static char * extension;
static char * fname;
static FILE *fin;
static char *path;
static int sl;

static int exclusive_mode = 1;

/* These 2 can be altered somewhat */
static int woutLatency = 1200;	/* in msec; ignored in exclusive mode  */
/* WOUT_CHUNKSIZE *MUST* be divisible by 8 */
#define WOUT_CHUNKSIZE 2040 //80000 //115200

void build_waveformat(void)
{
	memset(&wfe, 0, sizeof(WAVEFORMATEXTENSIBLE));
	wf = &wfe.Format;
	wf->wFormatTag = WAVE_FORMAT_PCM;
	wf->nChannels = channels;
	wf->nSamplesPerSec = sample_rate;
	if (bps == 24) {
		wfe.Samples.wValidBitsPerSample = bps;
		wfe.dwChannelMask = SPEAKER_FRONT_LEFT | SPEAKER_FRONT_RIGHT;
		wfe.SubFormat = KSDATAFORMAT_SUBTYPE_PCM;
		wf->wFormatTag = WAVE_FORMAT_EXTENSIBLE;
		wf->cbSize = sizeof(WAVEFORMATEXTENSIBLE) - sizeof(WAVEFORMATEX);
		wf->nBlockAlign = channels * 32 / 8;
		wf->nAvgBytesPerSec = wf->nSamplesPerSec * wf->nBlockAlign;
		wf->wBitsPerSample = 32;
	} else {
		wf->nBlockAlign = channels * bps / 8;
		wf->nAvgBytesPerSec = wf->nSamplesPerSec * wf->nBlockAlign;
		wf->wBitsPerSample = bps;
		wf->cbSize = 0;
	}

}

void touchmemory(void *start, void *end)
{
	while (end > start) {
		dummycheck ^= *(int *) end;
		end = ((FLAC__uint8 *) end) - (1 << 12);
	}
}

// Exclusive mode code copied and modified from: Exclusive-Mode Streams http://msdn.microsoft.com/en-us/library/dd370844(VS.85).aspx
int exclusive_openplay(void)
{
	#define REFTIMES_PER_SEC  10000000
	#define REFTIMES_PER_MILLISEC  10000
	#define EXIT_ON_ERROR(hres) if (FAILED(hres)) { goto Exit; }
	#define SAFE_RELEASE(punk) if ((punk) != NULL) { (punk)->Release(); (punk) = NULL; }

    HRESULT hr;
    REFERENCE_TIME minlatency = 0;
    IMMDeviceEnumerator *pEnumerator = NULL;
    IMMDevice *pDevice = NULL;
    IAudioClient *pAudioClient = NULL;
    IAudioRenderClient *pRenderClient = NULL;
    HANDLE hEvent = NULL;
    HANDLE hTask = NULL;
    UINT32 bufferFrameCount;
    BYTE *pData;
    DWORD flags = 0;
	DWORD taskIndex = 0;
	FLAC__uint8 *playhead, *buf_end;
	int chunksize, len, newpct;
	REFERENCE_TIME latency = 450 * REFTIMES_PER_MILLISEC;
	static int oldpct = -1;

	printf("Opening exclusive mode device...");
	playhead = buffer;
	buf_end = buffer + buffer_size;

	EXIT_ON_ERROR( hr = CoInitialize(NULL) );
    EXIT_ON_ERROR( hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_ALL, __uuidof(IMMDeviceEnumerator), (void**) &pEnumerator) );
	EXIT_ON_ERROR( hr = pEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &pDevice) );
	EXIT_ON_ERROR( hr = pDevice->Activate(__uuidof(IAudioClient), CLSCTX_ALL, NULL, (void**)&pAudioClient) );
	build_waveformat();
	//EXIT_ON_ERROR( hr = pAudioClient->IsFormatSupported(AUDCLNT_SHAREMODE_EXCLUSIVE, wf, NULL) );
	EXIT_ON_ERROR( hr = pAudioClient->GetDevicePeriod(NULL, &minlatency) );
	
    hr = pAudioClient->Initialize(AUDCLNT_SHAREMODE_EXCLUSIVE, AUDCLNT_STREAMFLAGS_EVENTCALLBACK, latency, latency, wf, NULL);
	if (hr == AUDCLNT_E_BUFFER_SIZE_NOT_ALIGNED) {
		EXIT_ON_ERROR( hr = pAudioClient->GetBufferSize(&bufferFrameCount) );
		latency = (REFERENCE_TIME) (REFTIMES_PER_SEC * ((REFERENCE_TIME) bufferFrameCount) / ((REFERENCE_TIME) wf->nSamplesPerSec));
		pAudioClient->Release();
		EXIT_ON_ERROR( hr = pDevice->Activate(__uuidof(IAudioClient), CLSCTX_ALL, NULL, (void**)&pAudioClient) );
		EXIT_ON_ERROR( hr = pAudioClient->Initialize(AUDCLNT_SHAREMODE_EXCLUSIVE, AUDCLNT_STREAMFLAGS_EVENTCALLBACK, latency, latency, wf, NULL) );
	} else
		EXIT_ON_ERROR(hr);

	hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    EXIT_ON_ERROR( hr = pAudioClient->SetEventHandle(hEvent) );
    pAudioClient->GetBufferSize(&bufferFrameCount);
	chunksize = bufferFrameCount * wf->nBlockAlign; 
    EXIT_ON_ERROR( hr = pAudioClient->GetService(__uuidof(IAudioRenderClient), (void**) &pRenderClient) );
	printf("OK - Latency %dms\n", ((REFERENCE_TIME) latency) / ((REFERENCE_TIME) REFTIMES_PER_MILLISEC));

	printf("Elevating priority to pro audio...");
    hTask = AvSetMmThreadCharacteristics(TEXT("Pro Audio"), &taskIndex);
    if (hTask == NULL)
		printf("NOT DONE - Expect possible buffer underruns\n");
	else
		printf("OK\n");
	
	printf("Touching memory...");
	touchmemory(buffer, buffer + buffer_size - 4);
	printf("OK\n");

	EXIT_ON_ERROR( hr = pRenderClient->GetBuffer(bufferFrameCount, &pData) );
		if (playhead + chunksize >= buf_end) {
			memset(pData, 0, chunksize);
			len = buf_end - playhead;
		} else
			len = chunksize;
		memcpy(pData, playhead, len);
		playhead += len;
        EXIT_ON_ERROR( hr = pRenderClient->ReleaseBuffer(bufferFrameCount, flags) );

	/* play */		
	printf("Playing...");
    EXIT_ON_ERROR( hr = pAudioClient->Start() );
    do {
        DWORD retval = WaitForSingleObject(hEvent, 5 * woutLatency); //was 2 * woutlatency
        if (retval != WAIT_OBJECT_0) {
			printf(" Timeout while waiting for event ");
            pAudioClient->Stop();
            hr = ERROR_TIMEOUT;
            goto Exit;
		}
		
		EXIT_ON_ERROR( hr = pRenderClient->GetBuffer(bufferFrameCount, &pData) );
		if (playhead + chunksize >= buf_end) {
			memset(pData, 0, chunksize);
			len = buf_end - playhead;
		} else
			len = chunksize;
		memcpy(pData, playhead, len);
		playhead += len;
        EXIT_ON_ERROR( hr = pRenderClient->ReleaseBuffer(bufferFrameCount, flags) );

		newpct = (int) (((float) (playhead - buffer)) / ((float) buffer_size) * 100.0f);
		if (newpct - oldpct > 0) {
			oldpct = newpct;
			printf("\rPlaying... %02d%%", newpct);
		}
		touchmemory(playhead, buf_end - 4);

	} while (playhead < buf_end);

    Sleep( (DWORD) ((REFERENCE_TIME) latency) / ((REFERENCE_TIME) REFTIMES_PER_MILLISEC) );
    EXIT_ON_ERROR( hr = pAudioClient->Stop() );

Exit:
    if (hEvent)
        CloseHandle(hEvent);
    if (hTask)
        AvRevertMmThreadCharacteristics(hTask);
    SAFE_RELEASE(pEnumerator)
    SAFE_RELEASE(pDevice)
    SAFE_RELEASE(pAudioClient)
    SAFE_RELEASE(pRenderClient)

	if FAILED(hr) {
		printf("ERROR ");
		if (HRESULT_FACILITY(hr) == FACILITY_AUDCLNT) {
			switch (hr) {
			case AUDCLNT_E_NOT_INITIALIZED: printf("not initialized"); break;
			case AUDCLNT_E_ALREADY_INITIALIZED: printf("already initialized"); break;
			case AUDCLNT_E_WRONG_ENDPOINT_TYPE: printf("wrong endpoint type"); break;
			case AUDCLNT_E_DEVICE_INVALIDATED: printf("device invalidated"); break;
			case AUDCLNT_E_NOT_STOPPED: printf("not stopped"); break;
			case AUDCLNT_E_BUFFER_TOO_LARGE: printf("buffer too large"); break;
			case AUDCLNT_E_OUT_OF_ORDER: printf("out of order"); break;
			case AUDCLNT_E_UNSUPPORTED_FORMAT: printf("unsupported format"); 
				printf("  Sample Rate     : %d", wfe.Format.nSamplesPerSec);
                printf("  Bits Per Sample : %d", wfe.Format.wBitsPerSample);
                printf("  Valid Bits/Samp : %d", wfe.Samples.wValidBitsPerSample);
                printf("  Channel Count   : %d", wfe.Format.nChannels);
                printf("  Block Align     : %d", wfe.Format.nBlockAlign);
                printf("  Avg. Bytes Sec  : %d", wfe.Format.nAvgBytesPerSec);
                printf("  Samples/Block   : %d", wfe.Samples.wSamplesPerBlock);
                printf("  Format cBSize   : %d", wfe.Format.cbSize);
                printf("  Channel Mask    : %d", wfe.dwChannelMask);		
				break;
			case AUDCLNT_E_INVALID_SIZE: printf("invalid size"); break;
			case AUDCLNT_E_DEVICE_IN_USE: printf("device in use"); break;
			case AUDCLNT_E_BUFFER_OPERATION_PENDING: printf("buffer operation pending"); break;
			case AUDCLNT_E_THREAD_NOT_REGISTERED: printf("thread not registered"); break;
			case AUDCLNT_E_EXCLUSIVE_MODE_NOT_ALLOWED: printf("exclusive mode not allowed"); break;
			case AUDCLNT_E_ENDPOINT_CREATE_FAILED: printf("endpoint create failed"); break;
			case AUDCLNT_E_SERVICE_NOT_RUNNING: printf("service not running"); break;
			case AUDCLNT_E_EVENTHANDLE_NOT_EXPECTED: printf("event handle not expected"); break;
			case AUDCLNT_E_EXCLUSIVE_MODE_ONLY: printf("exclusive mode only"); break;
			case AUDCLNT_E_BUFDURATION_PERIOD_NOT_EQUAL: printf("buffer duration period not equal"); break;
			case AUDCLNT_E_EVENTHANDLE_NOT_SET: printf("eventhandle not set"); break;
			case AUDCLNT_E_INCORRECT_BUFFER_SIZE: printf("incorrect buffer size"); break;
			case AUDCLNT_E_BUFFER_SIZE_ERROR: printf("buffer size error"); break;
			case AUDCLNT_E_CPUUSAGE_EXCEEDED: printf("cpu usage exceeded"); break;
			case AUDCLNT_E_BUFFER_ERROR: printf("buffer error"); break;
			case AUDCLNT_E_BUFFER_SIZE_NOT_ALIGNED: printf("buffer size not aligned"); break;
			case AUDCLNT_E_INVALID_DEVICE_PERIOD: printf("invalid device period"); break;
			}
		}
		printf("\n");
	} else
		printf(" DONE\n");
    return hr;
}


FLAC__StreamDecoderWriteStatus write_callback(const FLAC__StreamDecoder *decoder, const FLAC__Frame *frame, const FLAC__int32 * const buf[], void *client_data)
{
	unsigned int i, j;
	static FLAC__int8 *ptr = NULL;

	if (metaerror)
		return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;

	if (!ptr)
		ptr = (FLAC__int8 *) buffer;

	if (bps == 8) {
		for (i = 0; i < frame->header.blocksize; i++) {
			for (j = 0; j < channels; j++) {
				*((FLAC__int8 *) ptr) = (FLAC__int8) buf[j][i];
				ptr += sizeof(FLAC__uint8);
			}
		}
	} else if (bps == 16) {
		for (i = 0; i < frame->header.blocksize; i++) {
			for (j = 0; j < channels; j++) {
				*((FLAC__int16 *) ptr) = (FLAC__int16) buf[j][i];
				ptr += sizeof(FLAC__uint16);
			}
		}
	} else if (bps == 24) {
		for (i = 0; i < frame->header.blocksize; i++) {
			for (j = 0; j < channels; j++) {
				*((FLAC__int32 *) ptr) = buf[j][i] << 8;
				ptr += sizeof(FLAC__int32);
			}
		}
	}


	if (ptr > (FLAC__int8 * ) buffer + buffer_size) {
		fprintf(stderr, "ERROR: Overflow!\n");
		return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
	}

	return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
}

void error_callback(const FLAC__StreamDecoder *decoder, FLAC__StreamDecoderErrorStatus status, void *client_data)
{
	fprintf(stderr, "ERROR: FLAC decoder says: %s\n", FLAC__StreamDecoderErrorStatusString[status]);
}

int setup_waveout(void)
{
	MMRESULT err;
	int i;

	printf("Opening WaveOut device...");

	build_waveformat();
	err = waveOutOpen(&wout, WAVE_MAPPER, wf, (DWORD_PTR) NULL, (DWORD_PTR) NULL, CALLBACK_NULL);
	if (err != MMSYSERR_NOERROR) {
		fprintf(stderr, "ERROR: Cannot open waveout device\n");

		if (err == MMSYSERR_INVALHANDLE) fprintf(stderr, "Deeper: %s\n", "INVALHANDLE");
		if (err == MMSYSERR_BADDEVICEID) fprintf(stderr, "Deeper: %s\n", "BADDEVICEID");
		if (err == MMSYSERR_NODRIVER) fprintf(stderr, "Deeper: %s\n", "NODRIVER");
		if (err == MMSYSERR_NOMEM) fprintf(stderr, "Deeper: %s\n", "NOMEM");
		if (err == WAVERR_BADFORMAT) fprintf(stderr, "Deeper: %s\n", "BADFORMAT");
		if (err == WAVERR_SYNC) fprintf(stderr, "Deeper: %s\n", "WAVERR_SYNC");

		return 1;
	}

	woutBuffnum = ((wf->nAvgBytesPerSec * woutLatency / 1000) / WOUT_CHUNKSIZE) + 1;

	if (bps < 20)
		sl = woutLatency / woutBuffnum;              // to evala egw
	else
		sl=80;

	woutBuffers = (WAVEHDR *) malloc(woutBuffnum * sizeof(WAVEHDR));
	for (i = 0; i < woutBuffnum; i++) {
		memset(&woutBuffers[i], 0, sizeof(WAVEHDR));
		if (!(woutBuffers[i].lpData = (LPSTR) malloc(WOUT_CHUNKSIZE))) {
			fprintf(stderr, "ERROR: Cannot allocate memory for waveout buffer %d\n", i);
			return 1;
		}
		memset(woutBuffers[i].lpData, 0, WOUT_CHUNKSIZE);
		woutBuffers[i].dwBufferLength = WOUT_CHUNKSIZE;
		err = waveOutPrepareHeader(wout, &woutBuffers[i], sizeof(WAVEHDR));
		if (err != MMSYSERR_NOERROR) {
			fprintf(stderr, "ERROR: Cannot prepare waveout buffer %d\n", i);
			return 1;
		}
		woutBuffers[i].dwFlags |= WHDR_DONE;
	}

	printf("OK - Latency %dmsec, %d buffers of %d bytes\n", woutLatency, woutBuffnum, WOUT_CHUNKSIZE);
	return 0;
}

void playwave(void)
{
	int i, newpct;
	FLAC__uint8 *playhead, *buf_end;
	static int oldpct = -1;
	int len;
	MMRESULT err;

	playhead = buffer;

	buf_end = buffer + buffer_size;
	i = 0;
	printf("Playing...");
	do {
		while (woutBuffers[i].dwFlags & WHDR_DONE) {
			if (playhead + WOUT_CHUNKSIZE >= buf_end) {
				memset(woutBuffers[i].lpData, 0, WOUT_CHUNKSIZE);
				len = buf_end - playhead;
			} else
				len = WOUT_CHUNKSIZE;
			memcpy(woutBuffers[i].lpData, playhead, len);
			err = waveOutWrite(wout, &woutBuffers[i], sizeof(WAVEHDR));
			if (err != MMSYSERR_NOERROR) {
				fprintf(stderr, "\nError writing to device. Aborting.\n");
				return;
			}
			playhead += len;
			if (playhead >= buf_end)
				break;
			i = (i + 1) % woutBuffnum;
		}
		newpct = (int) (((float) (playhead - buffer)) / ((float) buffer_size) * 100.0f);
		if (newpct - oldpct > 0) {
			oldpct = newpct;
			printf("\rPlaying... %02d%%", newpct);
		}
		Sleep(sl);//(100);
		touchmemory(playhead, buf_end - 4);
	} while (playhead < buf_end);

	printf(" DONE\n");
}

void close_waveout(void)
{
	int flag, i;

	printf("Flushing buffers...");
	do {
		flag = true;
		for (i = 0; i < woutBuffnum; i++) {
			if (!(woutBuffers[i].dwFlags & WHDR_DONE)) {
				flag = false;
				Sleep(woutLatency);
			}
		}
	} while (flag == false);

	waveOutReset(wout);

	for (i = 0; i < woutBuffnum; i++) {
		if (waveOutUnprepareHeader(wout, &woutBuffers[i], sizeof(WAVEHDR)) != MMSYSERR_NOERROR)
			return;
		free(woutBuffers[i].lpData);
	}
	free(woutBuffers);
	waveOutClose(wout);
	free(buffer);
	buffer = NULL;
	printf("OK\n");
}

int soxplay(char *fl)
{
	char idx[1024];
	printf("Playing with SOX... \n");
	strcpy(idx,"start /b /REALTIME /WAIT ");
	strcat(idx,path);
	strcat(idx,"play \"");
	strcat(idx,fl);
	strcat(idx,"\" ");
	system(idx);
	printf("SOX DONE\n");
	return 0;
}

int wavplay(char *fn)
{
	int a;
	FLAC__uint32 starttime;
	starttime = GetTickCount();	
	fin = fopen(fn,"rb");

	if (!fin) {
			fprintf(stderr, "ERROR: File not found\n");
			metaerror = 1;
			return 1;
	}

	buffer = (FLAC__uint8 *) malloc((size_t) 1024);

	if (!buffer) {
			fprintf(stderr, "ERROR: Cannot allocate %dMB for buffer\n", buffer_size >> 20);
			metaerror = 1;
			fclose(fin);
			return 1;
	}

	if(
        fread(buffer, 1, 44, fin) != 44 ||
        memcmp(buffer, "RIFF", 4) ||
        memcmp(buffer+8, "WAVEfmt \020\000\000\000\001\000\002\000", 16) ||
        memcmp(buffer+32, "\004\000\020\000data", 8)
      ) {
         fprintf(stderr, "ERROR: invalid/unsupported WAVE file, only 16bps stereo WAVE in canonical form allowed\n");
         fclose(fin);
		 a=soxplay(fname);
         return 1;
        }

	sample_rate = ((((((unsigned)buffer[27] << 8) | buffer[26]) << 8) | buffer[25]) << 8) | buffer[24];
    channels = 2;
    bps = 16;
    total_samples = (((((((unsigned)buffer[43] << 8) | buffer[42]) << 8) | buffer[41]) << 8) | buffer[40]) / 4;
	buffer = NULL;
	buffer_size = total_samples * channels * bps / 8;
	buffer = (FLAC__uint8 *) malloc(((size_t) buffer_size));
		
	if (!buffer) {
			fprintf(stderr, "ERROR: Cannot allocate %dMB for buffer\n", buffer_size >> 20);
			metaerror = 1;
			fclose(fin);
			return 1;
	}

	printf("Input WAV stream: %s @ %.01fkHz, %dbpS\n", (channels == 1 ? "mono" : "stereo"), ((float) sample_rate / 1000.0), bps);
	printf("Allocated %dMB buffer\n", buffer_size >> 20);
		
	if (fread(buffer, (size_t) buffer_size, 1, fin) != 1) {
		fprintf(stderr, "ERROR loading file to memory... file\n");
        fclose(fin);
        return 1;
    }
	fclose(fin);

	if (exclusive_mode)
		exclusive_openplay();
	else {
		if (setup_waveout())	return 1;

		printf("Touching memory...");
		touchmemory(buffer, buffer + buffer_size - 4);
		printf("OK\n");

		playwave();
	
		close_waveout();
	}
	return 0;
}

int sox(char *fl)
{
	char idx[1024];
	printf("Using SOX... \n");
	strcpy(idx,"start /b /REALTIME /WAIT ");
	strcat(idx,path);
	strcat(idx,"sox \"");
	strcat(idx,fl);
	strcat(idx,"\" tmp.wav");
	system(idx);
	printf("Decoding complete, time to wave out...\n");
	return wavplay("tmp.wav");
}

void metadata_callback(const FLAC__StreamDecoder *decoder, const FLAC__StreamMetadata *metadata, void *client_data)
{
	if (metadata->type == FLAC__METADATA_TYPE_STREAMINFO) {
		total_samples = metadata->data.stream_info.total_samples;
		sample_rate = metadata->data.stream_info.sample_rate;
		channels = metadata->data.stream_info.channels;
		bps = metadata->data.stream_info.bits_per_sample;

		if (total_samples == 0) {
			fprintf(stderr, "ERROR: Input stream does not indicate the total samples in the file. Cannot play in memory\n");
			metaerror = sox(fname);
			return;
		}
		if (!(bps == 8 || bps == 16 || bps == 24)) {
			fprintf(stderr, "ERROR: Cannot process input stream with %dbpS (8, 16, 24 allowed)\n", bps);
			metaerror = sox(fname);
			return;
		}
		if (channels > 2) {
			fprintf(stderr, "ERROR: Cannot process input stream with more than 2 channels\n");
			metaerror = sox(fname);
			return;
		}

		buffer_size = total_samples * channels * (bps == 24 ? 32 : bps) / 8;
		buffer = (FLAC__uint8 *) malloc((size_t) buffer_size);
		if (!buffer) {
			fprintf(stderr, "ERROR: Cannot allocate %dMB for buffer\n", buffer_size >> 20);
			metaerror = 1;
			return;
		}

		printf("Input FLAC stream: %s @ %.01fkHz, %dbpS\n", (channels == 1 ? "mono" : "stereo"), ((float) sample_rate / 1000.0), bps);
		printf("Allocated %dMB buffer\n", buffer_size >> 20);
	}
}

int flacplay(char *fn)
{
	FLAC__bool ok = true;
	FLAC__StreamDecoder *decoder = 0;
	FLAC__StreamDecoderInitStatus init_status;
	FLAC__uint32 starttime;

	if ((decoder = FLAC__stream_decoder_new()) == NULL) {
		fprintf(stderr, "ERROR: Cannot create FLAC decoder\n");
		return 1;
	}
	FLAC__stream_decoder_set_md5_checking(decoder, true);
	init_status = FLAC__stream_decoder_init_file(decoder, fn, write_callback, metadata_callback, error_callback, 0);
	if (init_status != FLAC__STREAM_DECODER_INIT_STATUS_OK) {
		fprintf(stderr, "ERROR: Cannot initialize decoder: %s\n", FLAC__StreamDecoderInitStatusString[init_status]);
		return 1;
	}
	starttime = GetTickCount();
	if (FLAC__stream_decoder_process_until_end_of_stream(decoder))
		printf("Decoding OK - Took %.01fsec\n", ((float) GetTickCount() - starttime) / 1000.0f);
	else
		return 1;
	FLAC__stream_decoder_delete(decoder);

	if (exclusive_mode)
		exclusive_openplay();
	else {
		if (setup_waveout())	return 1;
	
		printf("Touching memory...");
		touchmemory(buffer, buffer + buffer_size - 4);
		printf("OK\n");

		playwave();
	
		close_waveout();
	}
	return 0;
}

int main(int argc, char *argv[])
{
	char idx[1024];
	path=argv[2];
	

	if (argc == 1) {
		fprintf(stderr, "usage: %s infile.flac\n", argv[0]);
		return 1;
	}

	printf("Trying to play %s\n",argv[1]);
	dot=strrchr(argv[1],'.');
	pos=dot-argv[1]+1;
	extension=argv[1]+pos;

	printf("Wasapi Mode Selected...\n");
				
    if (strcmp(extension,"flac") == 0 || strcmp(extension,"Flac") == 0 || strcmp(extension,"fLac") == 0 ||
		strcmp(extension,"flAc") == 0 || strcmp(extension,"flaC") == 0 || strcmp(extension,"FLac") == 0 ||
	    strcmp(extension,"FlAc") == 0 || strcmp(extension,"FlaC") == 0 || strcmp(extension,"fLAc") == 0 ||
	    strcmp(extension,"fLaC") == 0 || strcmp(extension,"flAC") == 0 || strcmp(extension,"FLAc") == 0 ||
	    strcmp(extension,"FLaC") == 0 || strcmp(extension,"fLAC") == 0 || strcmp(extension,"FLAC") == 0) {
		printf("Filetype detected: FLAC\n");
		fname=argv[1];
		return flacplay(fname);
	}
	else if (strcmp(extension,"wav") == 0 || strcmp(extension,"Wav") == 0 || strcmp(extension,"wAv") == 0
		 || strcmp(extension,"waV") == 0 || strcmp(extension,"WAv") == 0 || strcmp(extension,"wAV") == 0
		 || strcmp(extension,"WAV") == 0) {
		printf("Filetype detected: WAV\n");
		fname=argv[1];
		return wavplay(fname);
	}
	else if (strcmp(extension,"ape") == 0 || strcmp(extension,"Ape") == 0 || strcmp(extension,"aPe") == 0
		 || strcmp(extension,"apE") == 0 || strcmp(extension,"APe") == 0 || strcmp(extension,"aPE") == 0
		 || strcmp(extension,"APE") == 0) {
		printf("Filetype detected: APE\n");
		printf("Calling the Monkey... \n");
		fname=argv[1];
	    strcpy(idx,"start /b /REALTIME /WAIT ");
		strcat(idx,argv[2]);
		strcat(idx,"MAC.exe \"");
	    strcat(idx,fname);
		strcat(idx,"\" \"");
		strcat(idx,argv[2]);
	    strcat(idx,"tmp.wav\" -d");
	    system(idx);
	    printf("Monkey done, trying to wave out...\n");
		return wavplay("tmp.wav");
	}
	else if (strcmp(extension,"mpc") == 0 || strcmp(extension,"Mpc") == 0 || strcmp(extension,"mPc") == 0
		 || strcmp(extension,"mpC") == 0 || strcmp(extension,"MPc") == 0 || strcmp(extension,"mPC") == 0
		 || strcmp(extension,"MPC") == 0) {
		printf("Filetype detected: MPC\n");
		printf("Calling the mppdec... \n");
		fname=argv[1];
	    strcpy(idx,"start /b /REALTIME /WAIT ");
		strcat(idx,argv[2]);
		strcat(idx,"mppdec.exe --silent \"");
	    strcat(idx,fname);
		strcat(idx,"\" \"");
		strcat(idx,argv[2]);
	    strcat(idx,"tmp.wav");
	    system(idx);
	    printf("mppdec done, trying to wave out...\n");
		return wavplay("tmp.wav");
	}
	else if (strcmp(extension,"wma") == 0 || strcmp(extension,"Wma") == 0 || strcmp(extension,"wMa") == 0
		 || strcmp(extension,"wmA") == 0 || strcmp(extension,"WMa") == 0 || strcmp(extension,"wMA") == 0
		 || strcmp(extension,"WMA") == 0 || strcmp(extension,"aac") == 0 || strcmp(extension,"Aac") == 0
		 || strcmp(extension,"aAc") == 0 || strcmp(extension,"aaC") == 0 || strcmp(extension,"AAc") == 0
		 || strcmp(extension,"AaC") == 0 || strcmp(extension,"aAC") == 0 || strcmp(extension,"AAC") == 0 
		 || strcmp(extension,"m4a") == 0 || strcmp(extension,"M4a") == 0 || strcmp(extension,"M4A") == 0 ){
		printf("Filetype detected: WMA-AAC\n");
		printf("Calling ffmpeg... \n");
		fname=argv[1];
	    strcpy(idx,"start /b /REALTIME /WAIT ");
		strcat(idx,argv[2]);
		strcat(idx,"ffmpeg.exe -i \"");
	    strcat(idx,fname);
		strcat(idx,"\" -y \"");
		strcat(idx,argv[2]);
	    strcat(idx,"tmp.wav\"");
	    system(idx);
	    printf("FFmpeg done, trying to wave out...\n");
		return wavplay("tmp.wav");
	}
	else {
		fname=argv[1];
		return sox(fname);
	}
	
	return 0;
}